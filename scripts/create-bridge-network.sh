#!/bin/bash 
# WIP - not working yet
#if [ $# -ne 1 ]; then
#    echo "Usage: $0 <bridge-name>"
#    exit 1
#fi

#BRIDGE_NETWORK=${1}

nmcli con add ifname br500 type bridge con-name br500
nmcli con add type bridge-slave ifname eno1 master br500
nmcli con modify br500 ipv4.method manual ipv4.address "192.168.50.1/24" ipv4.gateway ""  ipv4.dns 10.46.0.31 ipv4.dns-search ""
cat >./bridge-br500.sh<<EOF
#!/bin/bash
nmcli con down eno1
nmcli con up br500
EOF
sh ./bridge-br500.sh

nmcli con show
nmcli con show --active

cat >bridge-500.xml <<EOF
<network>
  <name>br500</name>
  <forward mode="bridge"/>
  <bridge name="br500" />
</network>
EOF

virsh net-define bridge-500.xml

virsh net-start br500
virsh net-autostart br500

virsh net-list --all