# OpenShift GitOps

## Usage

Setup the cluster by running the following:

```
$ ./setup.sh
```

This will install the `Subscription`, repository and the `ClusterRoleBinding` so ArgoCD can manage the cluster.
