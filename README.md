# CVADLAB 
## Citrix Virtual Apps &amp; Desktops on OpenShift Virtualization (KubeVirt)

**IMPORTANT** NEEDS REVIEW
1. Deploy RHEL 8.6 on bare metal
2. Configure bare metal with network bridges -> [configure-network.md](configure-network.md)

## KubeConfig for `soctl`

The `kubeconfig-soctl` file is encrypted with Ansible Vault. The password should be known as it is used elsewhere (think iDrac). To use the `kubeconfig` you can run the following:

```sh
$ export KUBECONFIG=/path/to/kubeconfig-soctl
```

The full path is important so that if you change directories, you do not have to worry about `oc/kubectl` commands failing because the `kubeconfig` can no longer be found.
